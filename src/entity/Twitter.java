package entity;

import java.io.Serializable;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Builder(toBuilder = true)
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Table
public class Twitter implements Serializable {
    @Id
    private long id;

    @Column
    private String title;

    @Column
    private String content;
}
