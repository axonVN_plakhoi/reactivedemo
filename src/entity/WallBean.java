package entity;

import util.RequestContextUtil;
import util.TypeProvider;
import demo.TwitterAction;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.Subject;
import lombok.Getter;
import lombok.Setter;
import rx.RxDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Getter
public class WallBean {
    private static final String WALL_FORM = "wallForm";
    public List<Twitter> twitters =  new ArrayList<>();

    @Setter
    private Subject<RxDTO> subject;

    public WallBean(Subject<RxDTO> subject) {
        twitters.add(Twitter.builder().title("This is title").content("This is content").build());
        twitters.add(Twitter.builder().title("This is title 2").content("This is content 2").build());
        this.subject = subject;
        this.subscribe();
    }

    private void subscribe() {
        List<Consumer<RxDTO>> rxConsumers = Collections.singletonList(this.buildNewPostConsumer());
        Optional.ofNullable(this.getSubject()).ifPresent(
                b -> rxConsumers.forEach(b::subscribe));
    }

    private Consumer<RxDTO> buildNewPostConsumer() {
        return rx -> {
          if(TwitterAction.NEW_POST == rx.getAction()) {
              //Do something, add to twitter list and update the wall with new post
              Optional<Twitter> twitterOpt = TypeProvider.get(Twitter.class, rx.getTransferredObject());
              twitterOpt.ifPresent(twitter -> twitters.add(twitter));
              RequestContextUtil.updateUIComponent(WALL_FORM);
          }
        };
    }
}
