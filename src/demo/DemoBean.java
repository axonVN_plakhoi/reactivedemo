package demo;

import entity.Twitter;
import entity.WallBean;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import lombok.Getter;
import org.apache.commons.lang3.SerializationUtils;
import rx.RxDTO;
import rx.TypedRxDTO;

import java.util.Optional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@Getter
@ManagedBean(name = "demoBean")
@SessionScoped
public class DemoBean {
    private Subject<RxDTO> subject;
    private WallBean wallBean;
    private Twitter newTwitter;

    public DemoBean() {
        this.newTwitter = new Twitter();
        this.subject = BehaviorSubject.create();
        this.wallBean = new WallBean(subject);
    }

    //call from UI to update the wallBean
    public void triggerUpdateWall() {
        Twitter twitter = SerializationUtils.clone(this.newTwitter);
        RxDTO rxDTO = new TypedRxDTO(TwitterAction.NEW_POST, Twitter.class, twitter);
        Optional.ofNullable(this.subject).ifPresent(v -> v.onNext(rxDTO));
    }
}
