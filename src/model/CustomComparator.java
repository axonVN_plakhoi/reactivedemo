package model;


public interface CustomComparator<T> {


    boolean areEquals(T obj1, T obj2);

    Class<T> getGenericType();
}
