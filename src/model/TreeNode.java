package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
public class TreeNode<T extends Serializable> implements Serializable{
    private static final long serialVersionUID = 1L;
    private T data;
    private TreeNode<T> parent;
    private List<TreeNode<T>> children;

    public TreeNode(T data) {
        this.data = data;
        this.children = new LinkedList<>();
    }

    public TreeNode<T> addChild(T child) {
        TreeNode<T> childNode = new TreeNode<>(child);
        childNode.parent = this;
        return this.addChild(childNode);
    }

    public TreeNode<T> addChild(TreeNode<T> childNode) {
        childNode.parent = this;
        this.children.add(childNode);
        return childNode;
    }

    public boolean removeChild(TreeNode<T> childNode) {
        return this.children.remove(childNode);
    }

    public boolean isRoot() {
        return Objects.isNull(this.parent);
    }

    public boolean isLastChild() {
        return this.children.isEmpty();
    }
}
