package util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CollectionUtil {


    /**
     * Get first element from collection
     * @param collection The collection
     * @param <T> generic type of element
     * @return The Optional of first element, otherwise empty
     */
    public static <T> Optional<T> getFirstElement(Collection<T> collection) {
        if (Objects.isNull(collection)) {
            return Optional.empty();
        }
        return collection.stream().findFirst();
    }

    /**
     * Return the unmodifiable empty list if the @list is null.
     * Otherwise, return an unmodifiable view of the specified list.
     * @param list the list to be converted
     * @param <T> the class of the objects in the list
     * @return an unmodifiable list.
     */
    public static <T> List<T> getOrEmpty(List<T> list) {
        if (Objects.isNull(list)) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(list);
    }

    /**
     * Return the unmodifiable empty set if the @set is null.
     * Otherwise, return an unmodifiable view of the specified set.
     * @param set the set to be converted
     * @param <T> the class of the objects in the list
     * @return an unmodifiable set.
     */
    public static <T> Set<T> getOrEmpty(Set<T> set) {
        if (Objects.isNull(set)) {
            return Collections.emptySet();
        }
        return Collections.unmodifiableSet(set);
    }

    public static <K, V> Map<K, V> getOrEmpty(Map<K, V> map) {
        if (Objects.isNull(map)) {
            return Collections.emptyMap();
        }
        return Collections.unmodifiableMap(map);
    }
    /**
     * Return the unmodifiable empty set if the @set is null.
     * Otherwise, return an unmodifiable view of the specified set without any null element.
     * @param list the list to be converted
     * @param <T> the class of the objects in the list
     * @return an unmodifiable list.
     */
    public static <T> List<T> getNonNullOrEmpty(Collection<T> list) {
        if (Objects.isNull(list)) {
            return Collections.emptyList();
        }
        List<T> result = list.stream().filter(Objects::nonNull).collect(Collectors.toList());

        return Collections.unmodifiableList(result);
    }

    /**
     * Get the different fields of 2 lists.
     * Only in list A  + only in list B
     * @param listA the list A
     * @param listB the list B
     * @return the list of all different fields
     */
    public static <T> List<T> getDifferentFields(List<T> listA, List<T> listB) {
        List<T> returnList = new ArrayList<>();
        
        List<T> onlyA = new ArrayList<>(listA);
        onlyA.removeAll(listB);
        List<T> onlyB = new ArrayList<>(listB);
        onlyB.removeAll(listA);
        
        returnList.addAll(onlyA);
        returnList.addAll(onlyB);
        
        return Collections.unmodifiableList(returnList);
    }

    /**
     * Unionize the collections into one list.
     * @param lists the input collections.
     * @param <T> the class of the objects in the lists.
     * @return The result of lists unionization.
     */
    public static <T> List<T> union(Collection<T> list, Collection<T>... lists) {
        List<T> oneList = new ArrayList<>(list);
        oneList.addAll(Arrays.stream(lists)
                .flatMap(Collection::stream)
                .collect(Collectors.toList()));
        return Collections.unmodifiableList(oneList);
    }
    
    /**
     * Get the last item of the list.
     * @param list the list
     * @return the last item
     */
    public static <T> Optional<T> getLastItem(List<T> list) {
        return CollectionUtil.getOrEmpty(list).stream().reduce((T t1, T t2) -> t2);
    }
    
    /**
     * Check whether the collection is null or empty.
     * @param collection the collection
     * @return true or false
     */
    public static boolean isNullOrEmpty(Collection<?> collection) {
        return Objects.isNull(collection) || collection.isEmpty();
    }
    
    /**
     * Check whether the collection is not empty.
     * @param collection the collection
     * @return true if not empty
     */
    public static boolean isNotEmpty(Collection<?> collection) {
        return Objects.nonNull(collection) && !collection.isEmpty();
    }
    
    /**
     * Check whether the list contains ignore case the string.
     * @param collection the collection
     * @param string the string
     * @return true or false
     */
    public static boolean containsIgnoreCase(List<String> collection, String string) {
        if (StringUtil.isNullOrEmpty(string)) {
            return false;
        }
        
        return getOrEmpty(collection)
                .stream()
                .filter(Objects::nonNull)
                .anyMatch(string::equalsIgnoreCase);
    }

    /**
     * Collect items from collection by filter predicate.
     * @param collection the item collection
     * @param predicate the filter predicate
     * @return collection of specific type class
     */
    
    public static <T> List<T> collect(Collection<T> collection,Predicate<T> predicate){
        return getNonNullOrEmpty(collection)
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    /**
     * Collect items from collection with specific instance class.
     * @param collection the item collection
     * @param type the type to collect
     * @return collection of specific type class
     */
    public static <T> List<T> collect(Collection<?> collection, Class<T> type){
        return getNonNullOrEmpty(collection)
        .stream()
        .filter(type::isInstance)
        .map(type::cast)
        .collect(Collectors.toList());
    }
    
    /**
     * Collect items from collection by map function.
     * @param collection the item collection
     * @param function the map function
     * @return collection of specific type class
     */
    public static <T, U> List<U> map(Collection<T> collection, Function<T, U> function) {
        return getNonNullOrEmpty(collection)
                .stream()
                .map(function)
                .collect(Collectors.toList());
    }
    
    public static <T> List<T> getOrEmtpy(T[] array) {
        if (Objects.isNull(array)) {
            return Collections.emptyList();
        }
        return Arrays.asList(array);
    }

}
