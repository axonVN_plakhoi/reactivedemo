package util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.context.RequestContext;

import java.util.Objects;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class RequestContextUtil {
    /**
     * Execute the command.
     * If the request context is not initialized -> DO NOTHING.
     * @param command command to execute
     */
    public static void execute(String command) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        if (Objects.nonNull(requestContext)) {
            requestContext.execute(command);
        } else {
            log.warn(String.format("Request context is not initialized for executing command:%s", command));
        }
    }
    
    
    /**
     * Pass value to client and access it in js.
     * @param name : param name.
     * @param value: param value
     */
    public static void passParamToClient(String name,Object value){
        Optional.ofNullable(RequestContext.getCurrentInstance())
                .ifPresent(r -> r.addCallbackParam(name, value));
    }

    /**
     * Update the form on the UI.
     * If the request context is not initialized -> DO NOTHING.
     * @param componentId
     */
    public static void updateUIComponent(String componentId) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        if (Objects.nonNull(requestContext)) {
            requestContext.update(componentId);
        } else {
            log.warn(String.format("Request context is not initialized for update componentId:%s", componentId));
        }
    }

    public static void showDialog(String componentId) {
        RequestContextUtil.execute(String.format("PF('%s').show();", componentId));
    }

    public static void hideDialog(String componentId) {
        RequestContextUtil.execute(String.format("PF('%s').hide();", componentId));
    }
}
