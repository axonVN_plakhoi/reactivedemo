package util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TypeProvider {
    //private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(TypeProvider.class);
    public static <T> Optional<T> parse(Object object, Class<T> type) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            byte[] bytes = objectMapper.writeValueAsBytes(object);
            return Optional.ofNullable(objectMapper.readValue(bytes, type));
        } catch (JsonProcessingException e) {
            //log.error("Error on reading bytes from object: {}", e);
        } catch (IOException e) {
            //log.error("Error on parsing object from bytes: {}", e);
        }
        return Optional.empty();
    }

    public static <T> Optional<T> get(Class<T> type, Object object) {
        try {
            if (object instanceof Optional) {
                return getOptional(type, object);
            }
            return Optional.ofNullable(type.cast(object));
        } catch (ClassCastException e) {
            //log.error("Cannot cast " + type + " with " + object, e);
            return Optional.empty();
        }
    }

    @SuppressWarnings(value = "unchecked")
    public static <T> List<T> getCollection(Object obj){
        if(obj instanceof Collection){
            return CollectionUtil.getNonNullOrEmpty((Collection<T>)obj);
        }
        //log.error("Cannot cast to list");
        return Collections.emptyList();
    }

    @SuppressWarnings(value = "unchecked")
    private static <T> Optional<T> getOptional(Class<T> type, Object object) {
        Optional objectOpt = (Optional) object;
        return objectOpt.filter(type::isInstance).map(type::cast);
    }
}
