package util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor(access=AccessLevel.PRIVATE)
public final class StringUtil {

    public static final char CHAR_SPACE = ' ';
    public static final char UNIX_PATH_SEPARATOR = '/';
    
    /**
     * @param value : String
     * @return empty string if @value is null.
     * Trim whitespace at the beginning and end of @value.
     */
    public static String trimOrEmpty(String value) {
        return Optional.ofNullable(value)
                .map(String::trim)
                .orElse(StringUtils.EMPTY);
    }


    /**
     * Merge array of string to one string and separate by ' - '.
     * Example:
     * param: values["value1","value2"] -> result: "value1 - value2"
     *
     * @param values arrays of string
     * @return one string
     */
    public static String mergeStringSeparateByHyphen(String... values) {
        String[] newValues = Stream.of(values)
                .filter(StringUtils::isNoneEmpty)
                .toArray(String[]::new);

        if (newValues.length > 1) {
            return Stream.of(newValues).collect(Collectors.joining(" - "));
        }
        return Stream.of(newValues).collect(Collectors.joining(StringUtils.EMPTY));
    }

    /**
     * Automatically add double backslash \\ if value contain special character
     * <([{\^-=$!|]})?*+.>
     *
     * @param value :String
     * @return new value with double backslash before special character
     */
    public static String autoAddEscapeSymbol(String value) {
        return value.replaceAll("[\\<\\(\\[\\{\\\\\\^\\-\\=\\$\\!\\|\\]\\}\\)‌​\\?\\*\\+\\.\\>]", "\\\\$0");
    }
    
    /**
     * Get optional of text.
     * If text is blank will return Optional.empty.
     * @param text the text
     * @return Optional of text
     */
    public static Optional<String> getOptionalOrEmpty(String text) {
        return Optional.ofNullable(text)
                       .filter(StringUtils::isNotBlank);
    }
    
    /**
     * Remove the colon at the end of string.
     * @param label
     * @return label that remove the colon ending
     */
    public static String removeLastColon(String label){
        if (StringUtils.isBlank(label)) {
            return StringUtils.EMPTY;
        } else {
            return label.replaceFirst(":$","");
        }
    }
    
    public static String getOrNull(String text) {
        if (StringUtils.isBlank(text)) {
            return null;
        }
        return text;
    }
    
    public static boolean isNullOrEmpty(String string) {
        return Objects.isNull(string) || string.isEmpty();
    }
    
    public static boolean compareIgnoreCase(String firstVal,String secondVal) {
        return trimOrEmpty(firstVal).equalsIgnoreCase(trimOrEmpty(secondVal));
    }
    
    public static String trimToLowerCase(String val) {
        return trimOrEmpty(val).toLowerCase();
    }
}
