[Ivy]
162255FF719F98FE 3.20 #module
>Proto >Proto Collection #zClass
Tr0 Twitter Big #zClass
Tr0 B #cInfo
Tr0 #process
Tr0 @TextInP .resExport .resExport #zField
Tr0 @TextInP .type .type #zField
Tr0 @TextInP .processKind .processKind #zField
Tr0 @AnnotationInP-0n ai ai #zField
Tr0 @MessageFlowInP-0n messageIn messageIn #zField
Tr0 @MessageFlowOutP-0n messageOut messageOut #zField
Tr0 @TextInP .xml .xml #zField
Tr0 @TextInP .responsibility .responsibility #zField
Tr0 @StartRequest f0 '' #zField
Tr0 @EndTask f1 '' #zField
Tr0 @RichDialog f2 '' #zField
Tr0 @PushWFArc f3 '' #zField
Tr0 @PushWFArc f4 '' #zField
>Proto Tr0 Tr0 Twitter #zField
Tr0 f0 outLink start.ivp #txt
Tr0 f0 type demo.Data #txt
Tr0 f0 inParamDecl '<> param;' #txt
Tr0 f0 actionDecl 'demo.Data out;
' #txt
Tr0 f0 guid 162255FF87685E89 #txt
Tr0 f0 requestEnabled true #txt
Tr0 f0 triggerEnabled false #txt
Tr0 f0 callSignature start() #txt
Tr0 f0 caseData businessCase.attach=true #txt
Tr0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Tr0 f0 @C|.responsibility Everybody #txt
Tr0 f0 81 49 30 30 -21 17 #rect
Tr0 f0 @|StartRequestIcon #fIcon
Tr0 f1 type demo.Data #txt
Tr0 f1 441 49 30 30 0 15 #rect
Tr0 f1 @|EndIcon #fIcon
Tr0 f2 targetWindow NEW #txt
Tr0 f2 targetDisplay TOP #txt
Tr0 f2 richDialogId demo.Twitter #txt
Tr0 f2 startMethod start() #txt
Tr0 f2 type demo.Data #txt
Tr0 f2 requestActionDecl '<> param;' #txt
Tr0 f2 responseActionDecl 'demo.Data out;
' #txt
Tr0 f2 responseMappingAction 'out=in;
' #txt
Tr0 f2 isAsynch false #txt
Tr0 f2 isInnerRd false #txt
Tr0 f2 userContext '* ' #txt
Tr0 f2 216 42 112 44 0 -8 #rect
Tr0 f2 @|RichDialogIcon #fIcon
Tr0 f3 expr out #txt
Tr0 f3 111 64 216 64 #arcP
Tr0 f4 expr out #txt
Tr0 f4 328 64 441 64 #arcP
>Proto Tr0 .type demo.Data #txt
>Proto Tr0 .processKind NORMAL #txt
>Proto Tr0 0 0 32 24 18 0 #rect
>Proto Tr0 @|BIcon #fIcon
Tr0 f0 mainOut f3 tail #connect
Tr0 f3 head f2 mainIn #connect
Tr0 f2 mainOut f4 tail #connect
Tr0 f4 head f1 mainIn #connect
