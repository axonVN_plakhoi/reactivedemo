[Ivy]
1622D185C20359BB 3.20 #module
>Proto >Proto Collection #zClass
Ut0 UserInput Big #zClass
Ut0 B #cInfo
Ut0 #process
Ut0 @TextInP .resExport .resExport #zField
Ut0 @TextInP .type .type #zField
Ut0 @TextInP .processKind .processKind #zField
Ut0 @AnnotationInP-0n ai ai #zField
Ut0 @MessageFlowInP-0n messageIn messageIn #zField
Ut0 @MessageFlowOutP-0n messageOut messageOut #zField
Ut0 @TextInP .xml .xml #zField
Ut0 @TextInP .responsibility .responsibility #zField
Ut0 @StartRequest f0 '' #zField
Ut0 @EndTask f1 '' #zField
Ut0 @RichDialog f3 '' #zField
Ut0 @PushWFArc f4 '' #zField
Ut0 @PushWFArc f2 '' #zField
>Proto Ut0 Ut0 UserInput #zField
Ut0 f0 outLink start.ivp #txt
Ut0 f0 type demo.Data #txt
Ut0 f0 inParamDecl '<> param;' #txt
Ut0 f0 actionDecl 'demo.Data out;
' #txt
Ut0 f0 guid 1622D185C246BA67 #txt
Ut0 f0 requestEnabled true #txt
Ut0 f0 triggerEnabled false #txt
Ut0 f0 callSignature start() #txt
Ut0 f0 caseData businessCase.attach=true #txt
Ut0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Ut0 f0 @C|.responsibility Everybody #txt
Ut0 f0 81 49 30 30 -21 17 #rect
Ut0 f0 @|StartRequestIcon #fIcon
Ut0 f1 type demo.Data #txt
Ut0 f1 337 49 30 30 0 15 #rect
Ut0 f1 @|EndIcon #fIcon
Ut0 f3 targetWindow NEW #txt
Ut0 f3 targetDisplay TOP #txt
Ut0 f3 richDialogId demo.UserInput #txt
Ut0 f3 startMethod start() #txt
Ut0 f3 type demo.Data #txt
Ut0 f3 requestActionDecl '<> param;' #txt
Ut0 f3 responseActionDecl 'demo.Data out;
' #txt
Ut0 f3 responseMappingAction 'out=in;
' #txt
Ut0 f3 isAsynch false #txt
Ut0 f3 isInnerRd false #txt
Ut0 f3 userContext '* ' #txt
Ut0 f3 168 42 112 44 0 -8 #rect
Ut0 f3 @|RichDialogIcon #fIcon
Ut0 f4 expr out #txt
Ut0 f4 111 64 168 64 #arcP
Ut0 f2 expr out #txt
Ut0 f2 280 64 337 64 #arcP
>Proto Ut0 .type demo.Data #txt
>Proto Ut0 .processKind NORMAL #txt
>Proto Ut0 0 0 32 24 18 0 #rect
>Proto Ut0 @|BIcon #fIcon
Ut0 f0 mainOut f4 tail #connect
Ut0 f4 head f3 mainIn #connect
Ut0 f3 mainOut f2 tail #connect
Ut0 f2 head f1 mainIn #connect
