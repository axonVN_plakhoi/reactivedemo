package demo.Twitter;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class TwitterData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class TwitterData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 6965419348366646323L;

  private demo.DemoBean demoBean;

  /**
   * Gets the field demoBean.
   * @return the value of the field demoBean; may be null.
   */
  public demo.DemoBean getDemoBean()
  {
    return demoBean;
  }

  /**
   * Sets the field demoBean.
   * @param _demoBean the new value of the field demoBean.
   */
  public void setDemoBean(demo.DemoBean _demoBean)
  {
    demoBean = _demoBean;
  }

}
